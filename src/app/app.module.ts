import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpModule} from '@angular/http';
import {appRoutingModule} from './app.routing';

import {AppComponent} from './app.component';
import {UserModule} from './user/user.module';
import {AdminModule} from './admin/admin.module';
import {ItgMaterialModule} from './material.module';

import 'hammerjs';

@NgModule({
  declarations: [
    AppComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    appRoutingModule,
    ItgMaterialModule,
    UserModule,
    AdminModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
