import { Component, OnInit } from '@angular/core';
import { AdminService } from './../../../services/admin.service';

@Component({
  selector: 'itg-manage-posts',
  templateUrl: './manage-posts.component.html',
  styleUrls: ['./manage-posts.component.css']
})
export class ManagePostsComponent implements OnInit {

  resPosts: any;
    slideData: any ='';
    message: string = '';
    isActive: boolean = false;
  constructor(private _adminService: AdminService) { }

  ngOnInit() {
    this.getAllUnverifiedPosts();
    }

    getAllUnverifiedPosts() :void{
        this._adminService.getUnverifiedPosts().subscribe(
            result => {
                this.message = result.message;
                this.resPosts = result.posts;
            });
    }

    verifyPost(id: number): void{
      this._adminService.verifyPostById(id).subscribe(
        result => {
          // console.log(result);
        });
    }

}
