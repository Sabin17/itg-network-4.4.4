import { Component, OnInit } from '@angular/core';
import { AdminService } from './../../../services/admin.service';

@Component({
    selector: 'manage-users',
    templateUrl: 'manage-users.component.html',
    styleUrls: ['./manage-users.component.scss']
})
export class ManageUsersComponent implements OnInit {

    resUsers: any;
    slideData: any ='';
    message: string = '';
    isActive: boolean = false;

     constructor(private _adminService: AdminService) {
        this.getAllUnverifiedUsers();
    }

    ngOnInit() { }

     getAllUnverifiedUsers(){
        this._adminService.getUnverifiedUsers().subscribe(
            result => {
                this.message = result.message;
                this.resUsers = result.users;
            });
    }

    verifyUser(id: number){
        this.slideData = id;
        this.isActive = true;
        this._adminService.verifyUserById(id).subscribe(
            result => {
                // console.log(result)
            });
    }
}
