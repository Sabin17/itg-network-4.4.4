import { Component, OnInit } from '@angular/core';
import { PostService } from './../../../services/post.service';

@Component({
  selector: 'itg-manage-feedback',
  templateUrl: './manage-feedback.component.html',
  styleUrls: ['./manage-feedback.component.scss']
})
export class ManageFeedbackComponent implements OnInit {

  feedbacks: any;
  countfeedback: any;
  constructor(private _postService:PostService) { }

  ngOnInit() {
    this.getAllFeedback();
  }
  getAllFeedback(){
    this._postService.getAllFeedbacks().subscribe(
      result => {
        this.feedbacks = result.datas;
        this.countfeedback = result.countfeedback;
      });
  }

}
