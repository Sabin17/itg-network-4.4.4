import { Component, OnInit } from '@angular/core';
import { AdminService } from './../../../services/admin.service';

@Component({
  selector: 'itg-admin-analytics',
  templateUrl: './admin-analytics.component.html',
  styleUrls: ['./admin-analytics.component.scss']
})
export class AdminAnalyticsComponent implements OnInit {

  numericValue: any;
  constructor(private _adminService: AdminService) { }

  ngOnInit() {
    this.getAllAnalytics();
  }

  getAllAnalytics(): void{
    this._adminService.getAllAdminAnalytics().subscribe(
      result => {
        this.numericValue = result;
      }
    )
  }

}
