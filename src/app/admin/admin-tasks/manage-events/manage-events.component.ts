import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PostService} from './../../../services/post.service';
import {AdminService} from './../../../services/admin.service';


@Component({
  selector: 'itg-manage-events',
  templateUrl: './manage-events.component.html',
  styleUrls: ['./manage-events.component.scss']
})
export class ManageEventsComponent implements OnInit {
  events: any;
  message: any;

  eventForm: FormGroup;

  constructor(private _postService: PostService,
              private fb: FormBuilder,
              private _adminService: AdminService) {

  }

  eventPostForm() {
    this.eventForm = this.fb.group({
      event_title: ['', Validators.required],
      event_description: ['', Validators.required],
      event_location: ['', Validators.required],
      event_image: [''],
      start_datetime: [''],
      end_datetime: [''],
      user_id: [13]
    })
  }

  ngOnInit() {
    this.getAllEventLists();
    this.eventPostForm();
  }

  getAllEventLists(): void {
    this._postService.getAllEventPosts().subscribe(
      result => {
        this.events = result.events;
        this.message = result.message;
      });
  }

  createNewEvent(event) {
    const eventData = this.eventForm.value;
    this._adminService.createEventPost(eventData).subscribe(
      result => {
        console.log('');
      }
    );
  }

}
