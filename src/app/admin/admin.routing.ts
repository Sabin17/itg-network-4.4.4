import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ManageUsersComponent} from './admin-tasks/manage-users/manage-users.component';
import {AdminAnalyticsComponent} from './admin-tasks/admin-analytics/admin-analytics.component';
import {ManagePostsComponent} from './admin-tasks/manage-posts/manage-posts.component';
import {ManageEventsComponent} from './admin-tasks/manage-events/manage-events.component';
import {ManageFeedbackComponent} from './admin-tasks/manage-feedback/manage-feedback.component';
import {AdminDashboardComponent} from './admin-dashboard.component';

const adminRoutes: Routes = [
  {
    path: '',
    component: AdminDashboardComponent,
    children: [
      {
        path: 'users',
        component: ManageUsersComponent
      },
      {
        path: 'analytics',
        component: AdminAnalyticsComponent
      },
      {
        path: 'posts',
        component: ManagePostsComponent
      },
      {
        path: 'events',
        component: ManageEventsComponent
      },
      {
        path: 'feedback',
        component: ManageFeedbackComponent
      },
    ]
  }
];

export const adminRoutingModule: ModuleWithProviders = RouterModule.forChild(adminRoutes);
