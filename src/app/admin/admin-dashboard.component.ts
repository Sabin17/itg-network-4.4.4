import { Component, OnInit } from '@angular/core';
import { AdminService } from './../services/admin.service';
import { PostService } from './../services/post.service';

@Component({
    selector: 'admin-dashboard',
    templateUrl: 'admin-dashboard.component.html',
    styleUrls: ['./admin-dashboard.component.scss'],
    providers: [AdminService, PostService]
})
export class AdminDashboardComponent implements OnInit {

    resUsers: any;
    slideData: any ='';
    message: string = '';
    isActive: boolean = false;

    constructor(private _adminService: AdminService) { 
    }

    ngOnInit() {
        
     }

   
}