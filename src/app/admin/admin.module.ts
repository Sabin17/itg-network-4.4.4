import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {ItgMaterialModule} from '../material.module';
import {ModalModule} from 'ngx-modal';
import {AdminDashboardComponent} from './admin-dashboard.component';
import {AdminNavigationComponent} from './admin-navigation/admin-navigation.component';
import {AdminAnalyticsComponent} from './admin-tasks/admin-analytics/admin-analytics.component';
import {ManageEventsComponent} from './admin-tasks/manage-events/manage-events.component';
import {ManagePostsComponent} from './admin-tasks/manage-posts/manage-posts.component';
import {ManageUsersComponent} from './admin-tasks/manage-users/manage-users.component';
import {ManageFeedbackComponent} from './admin-tasks/manage-feedback/manage-feedback.component';
import {adminRoutingModule} from './admin.routing';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    adminRoutingModule,
    ItgMaterialModule,
    ModalModule
  ],
  exports: [],
  declarations: [
    AdminDashboardComponent,
    AdminNavigationComponent,
    AdminAnalyticsComponent,
    ManageEventsComponent,
    ManagePostsComponent,
    ManageUsersComponent,
    ManageFeedbackComponent,
  ],
  providers: [],
})
export class AdminModule {
}
