//guard is also a service so we need injectables in this file too
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

//use CanActivate interface to this guard to secure our routes
@Injectable()
export class AuthGuard implements CanActivate{

    // injection router of type Route
    constructor(private router: Router){

    }

    //method override gareko
    canActivate(){
        //already currentUser vanne cha ki chaina vanera check gareko re 
        if(localStorage.getItem('currentUser')){
            //logged in so return true
            //its something like -> checking the session in php
            return true;
        }else{
            //not logged in so redirect to login page
            this.router.navigate(['/login']);
            return false;
        }
    }
}