export interface User{
    email:string;
    country_id:number;
    province_id:number;
    zone_id:number;
    district_id:number;
    city_id:number;
    local_address:string;

    fname:string;
    mname:string;
    lname:string;

    gender:string;
    contact_no:string;
    mobile_no:string;

    course_id:number;
    whyus: string;
}