import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {appRoutingModule} from '../app.routing';
import {ItgMaterialModule} from '../material.module';
import {ModalModule} from 'ngx-modal';

// components
import {NavigationComponent} from './components/navigation/navigation.component';
import {HomeComponent} from './components/home/home.component';
import {RegisterComponent} from './components/register/register.component';
import {LoginComponent} from './components/login/login.component';

// profile component
import {ProfileComponent} from './components/profile/profile.component';
import {ProfileInfoBasicComponent} from './components/profile/profile-info-basic/profile-info-basic.component';
import {ProfileInfoCompleteComponent} from './components/profile/profile-info-complete/profile-info-complete.component';
import {PersonalDetailsComponent} from './components/profile/profile-info-complete/personal-details/personal-details.component';
import {ActivitiesDetailsComponent} from './components/profile/profile-info-complete/activities-details/activities-details.component';
import {ProjectDetailsComponent} from './components/profile/profile-info-complete/project-details/project-details.component';
import {SettingsComponent} from './components/profile/profile-info-complete/settings/settings.component';
import {ProfileNavComponent} from './components/profile/profile-info-complete/profile-nav/profile-nav.component';

import {DashboardComponent} from './components/dashboard/dashboard.component';
import {ForumDetailComponent} from './components/dashboard/forum-detail/forum-detail.component';
import {ForumPostComponent} from './components/dashboard/forum-post/forum-post.component';
import {ForumListComponent} from './components/dashboard/forum-list/forum-list.component';
import {ForumFilterListComponent} from './components/dashboard/forum-filter-list/forum-filter-list.component';

import {EventComponent} from './components/event/event.component';
import {EventDetailComponent} from './components/event/event-detail/event-detail.component';

import {MembersComponent} from './components/members/members.component';
import {FooterComponent} from './components/footer/footer.component';
import {FeedbackComponent} from './components/feedback/feedback.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ItgMaterialModule,
    appRoutingModule,
    ModalModule,
    BrowserAnimationsModule,

  ],
  exports: [
    NavigationComponent,
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    NavigationComponent,
    ProfileComponent,
    ProfileInfoBasicComponent,
    ProfileInfoCompleteComponent,
    PersonalDetailsComponent,
    ActivitiesDetailsComponent,
    ProjectDetailsComponent,
    SettingsComponent,
    ProfileNavComponent,
    HomeComponent,
    DashboardComponent,
    ForumListComponent,
    ForumFilterListComponent,
    ForumDetailComponent,
    ForumPostComponent,
    EventComponent,
    EventDetailComponent,
    MembersComponent,
    FooterComponent,
    FeedbackComponent,
  ],
  providers: [],
})
export class UserModule {
}
