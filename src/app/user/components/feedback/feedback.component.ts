import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MdSnackBar, MdDialog, MdDialogRef} from '@angular/material';
import { Http, Headers } from '@angular/http';
import { PostService } from './../../../services/post.service';
declare var swal: any;


@Component({
  selector: 'itg-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss'],
})
export class FeedbackComponent implements OnInit {

  feedbackForm: FormGroup;
  photo: File;

  constructor(
      private fb: FormBuilder,
      private http: Http,
      private _postService: PostService,
      private router: Router,
      public snackBar: MdSnackBar
    ) {
      console.log('feedback component started')
        // this.getAllFeedback();
    }

  ngOnInit() {
    this.myFeedbackForm();
  }

  myFeedbackForm(){
    this.feedbackForm = this.fb.group({
      feedback_title: ['', Validators.required],
      feedback_user: ['', Validators.required],
      feedback_detail: ['', Validators.required],
    })
  }

  showAlert(){
        swal(
          'Good job!',
          'You clicked the button!',
          'success'
        )
  }

 // postPhoto(event: EventTarget) {
 //      let eventObj: MSInputMethodContext = <MSInputMethodContext> event;
 //      let target: HTMLInputElement = <HTMLInputElement> eventObj.target;
 //      let files: FileList = target.files;
 //      this.photo = files[0];
 //      console.log(this.photo);
 //  }

  submitFeedback(feedbackForm){
    // console.log(this.jobseeker);
    //   let formdata: FormData= new FormData();
    //     formdata.append('file', this.photo);
    //     formdata.append('feedback', JSON.stringify(this.feedbackForm.value));
      this._postService.createFeedbackPost(this.feedbackForm.value).subscribe(
        res => {
          this.feedbackForm.reset();
        })
  }

  getAllFeedback() {
    this._postService.getAllFeedbacks().subscribe(
      result => {
        // console.log(result);
    });
  }

  openSnackBar() {
    if (this.feedbackForm.valid === true) {
        // this.snackBar.open('Congratulation !!! Your feedback is been submitted','', {
        //     duration: 1500,
        //   });
        swal(
          'Congratulation !!',
          'Your form is been submitted successfully',
          'success'
        );
    }else {
        // this.snackBar.open('Invalid Form !!!','', {
        //     duration: 1500,
        //   });
         swal(
          'Sorry !!',
          'You can not add any feedback with invalid form',
          'error'
        );
    }
  }


}

