import {Component, Input, OnInit} from '@angular/core';
import { MembersService } from '../../../services/members.service';
import {SearchService} from '../../../services/search.service';
import {customLoadingAnimation, sectionAnimation} from '../../../animations/slide.animation';


@Component({
  selector: 'itg-members',
  templateUrl: 'members.component.html',
  styleUrls: ['members.component.scss'],
  animations: [sectionAnimation, customLoadingAnimation]
})
export class MembersComponent implements OnInit {

  founders: any;
  mentors: any;
  submentors: any;
  membersonly: any;

  allmembers: any[];
  username: any;

  private profile_img: string;

  // @Input()
  constructor( private _membersService: MembersService, private _searchService: SearchService) {
    setTimeout(() => {
      this.showMembers();
    }, 3000);
  }

  showMembers() {
    this._membersService.getMembers().subscribe(res => {
      this.founders = res.founders;
      this.mentors = res.mentors;
      this.submentors = res.submentors;
      this.membersonly = res.membersonly;
      this.profile_img = res.member_profile;
      this.allmembers = this.founders.concat(this.mentors).concat(this.submentors).concat(this.membersonly);
    });
  }
  searchMembers() {
    if (this.username.length === 0) {
     this.showMembers();
    }else {
    this._searchService.updateUser(this.username);
    this._searchService.searchMembers().subscribe(res => {
      this.allmembers = res.datas;
    });
    }
  }

  ngOnInit() {
  }

}
