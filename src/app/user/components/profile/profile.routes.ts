import { Routes } from '@angular/router';
import { ProfileInfoBasicComponent } from './profile-info-basic/profile-info-basic.component';
import { ProfileInfoCompleteComponent } from './profile-info-complete/profile-info-complete.component';
import { PROFILE_INFO_ROUTES } from './profile-info-complete/profile-info.routes';

export const PROFILE_ROUTES: Routes = [
  {
    path: '',
    component: ProfileInfoBasicComponent
  },
  {
    path: '',
    component: ProfileInfoCompleteComponent,
    children: PROFILE_INFO_ROUTES
  }
];
