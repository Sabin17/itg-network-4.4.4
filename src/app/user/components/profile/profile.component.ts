import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../services/profile.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'itg-profile',
  templateUrl: 'profile.component.html',
  styleUrls: ['profile.component.scss'],
})
export class ProfileComponent implements OnInit {

  constructor(private _profileService: ProfileService, private _activatedRoute: ActivatedRoute) {
  }

  showMore(event: any) {
    event.preventDefault();
    event.srcElement.parentElement.previousElementSibling.lastElementChild.classList.toggle('active');
  }
  ngOnInit() {
  }
}
