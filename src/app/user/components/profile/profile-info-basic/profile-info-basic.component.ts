import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {ProfileService} from '../../../../services/profile.service';
import {AuthenticationService} from '../../../../services/authentication.service';

@Component({
  selector: 'itg-profile-info-basic',
  templateUrl: 'profile-info-basic.component.html',
  styleUrls: ['profile-info-basic.component.scss'],
})
export class ProfileInfoBasicComponent implements OnInit, AfterViewInit {

  profileRes: any;
  id: any;
  private profile_img: any;
  userdata: any;
  user_id: number;

  constructor(private router: Router,
              private _profileService: ProfileService,
              private _activatedRoute: ActivatedRoute,
              private _authService: AuthenticationService) {
  }

  showProfile(id) {
    this._profileService.getUsersById(id).subscribe(res => {
      this.profileRes = res.users;
      this.profile_img = res.profile_image;
    });
  }

  showOthersProfile(id) {
    this._profileService.getOthersProfile(id).subscribe(res => {
      this.profileRes = res.users;
      this.profile_img = res.profile_image;
    });
  }

  ngOnInit() {}
  ngAfterViewInit() {
    this._activatedRoute.params.subscribe((param: any) => {
      this.id = param['id'];
      this.showProfile(this.id);
    });
  }

}
