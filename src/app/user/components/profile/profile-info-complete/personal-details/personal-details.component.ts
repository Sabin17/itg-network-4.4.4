import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {ProfileService} from '../../../../../services/profile.service';
import {slideAnimation, customLoadingAnimation} from '../../../../../animations';

@Component({
  selector: 'itg-personal-details',
  templateUrl: 'personal-details.component.html',
  styleUrls: ['personal-details.component.scss'],
  animations: [slideAnimation, customLoadingAnimation]
})

export class PersonalDetailsComponent implements OnInit, AfterViewInit{
  id: any;
  profileRes: any;
  experienceRes: any;
  educationRes: any;
  skillsRes: any;
  specExperience: any;

  // user id for POSTing data's
  userData = JSON.parse(localStorage.getItem('currentUser'));
  userId = '';

  allInfo: any;

  constructor(private router: Router,
              private _profileService: ProfileService,
              private _activatedRoute: ActivatedRoute) {

  }

  showAll(newid) {
    const dummy1 = this._profileService.getUsersById(newid).subscribe(res => {
      this.profileRes = res.users;
    });
    const dummy2 = this._profileService.getExperience(newid).subscribe(res => {
      this.experienceRes = res.experience;
    });
    const dummy3 = this._profileService.getEducation(newid).subscribe(res => {
      this.educationRes = res.datas;
    });
    const dummy4 = this._profileService.getSkills(newid).subscribe(res => {
      this.skillsRes = res.skills;
    });
    setTimeout(() => {
      this.allInfo = [dummy1, dummy2, dummy3, dummy4];
    }, 3000);
  }

  // Subscribing specific data
  showSpecificExperience(user_id, exp_id) {
    this._profileService.getSpecificExperience(user_id, exp_id).subscribe(res => {
      this.specExperience = res.datas;
      // this.router.navigate(['/profile/'+user_id+'/settings']);
    });
  }

  // navigation function
  onNavigate(id) {
    this.router.navigate(['/profile/' + id + '/settings']).then();
  }

  ngOnInit() {
    this._activatedRoute.params.subscribe((param: any) => {
      this.id = +param ['id'];
      if (this.userId !== this.id || !this.userId) {
        this.showAll(this.id);
      }
    });
    // if (this.userId === this.id) {
    //   this.showAll(this.id);
    // }
    if (this.userData) {
      this.userId = this.userData.user_id;
    }
  }
  ngAfterViewInit() {

  }
}
