import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {ProfileService} from '../../../../../services/profile.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {slideAnimation, customLoadingAnimation} from '../../../../../animations';


@Component({
  selector: 'itg-project-details',
  templateUrl: 'project-details.component.html',
  styleUrls: ['project-details.component.scss'],
  animations: [slideAnimation, customLoadingAnimation]
})
export class ProjectDetailsComponent implements OnInit, OnDestroy {
  // variables to get the parent route id
  sub: any;
  parentRouteId: number;
  // variable to get the store the project result
  projectRes: any;
  // user id for POSTing datas
  userData = JSON.parse(localStorage.getItem('currentUser'));
  userId: number;
  // creating form
  projectForm: FormGroup;
  noResult: boolean = false;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private _profileService: ProfileService,
              private fb: FormBuilder,) {
  }

  // navigation function
  onNavigate(id) {
    this.router.navigate(['/profile/' + id + '/settings']).then();
  }

  // function to get the project result
  showProjects(id) {
    this._profileService.getOthersProject(id).subscribe(res => {
      // check if the result is null
      if (res.status === 503) {
        this.noResult = true;
      }
      this.projectRes = res.projects;
    });
  }

  // creating form
  createProject() {
    this.projectForm = this.fb.group({
      project_title: ['', Validators.required],
      project_description: ['', Validators.required],
      project_duration: ['', Validators.required],
      // project_status: ['',  Validators.required],
    });
  }

  ngOnInit() {
    // to get parent's id form url
    this.sub = this.route.parent.params.subscribe(params => {
      this.parentRouteId = +params['id'];
      this.showProjects(this.parentRouteId);
    });
    if (this.userData) {
      this.userId = this.userData.user_id;
    }
    this.createProject();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  // end

}
