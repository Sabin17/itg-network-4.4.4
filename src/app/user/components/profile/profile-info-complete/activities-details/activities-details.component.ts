import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProfileService} from '../../../../../services/profile.service';
import {slideAnimation, customLoadingAnimation} from '../../../../../animations';

@Component({
  selector: 'itg-activities-details',
  templateUrl: 'activities-details.component.html',
  styleUrls: ['activities-details.component.scss'],
  animations: [slideAnimation, customLoadingAnimation]
})

export class ActivitiesDetailsComponent implements OnInit {

  // Variables to find the parent route id
  sub: any;
  parentRouteId: any;
  userData = JSON.parse(localStorage.getItem('currentUser'));
  userId = '';
  // variable to store ctivities result
  activities: any;
  noResult: boolean = false;

  constructor(private route: ActivatedRoute, private _profileService: ProfileService) {

  }

  ngOnInit() {
    this.sub = this.route.parent.params.subscribe(params => {
      this.parentRouteId = +params['id'];
    });
    if (this.userData) {
      this.userId = this.userData.user_id;
    }
    this.getAllUserActivities();
  }

  getAllUserActivities() {
    this._profileService.getAllActivitiesOfUser(this.parentRouteId).subscribe(res => {
      if (res.status === 503) {
        this.noResult = true;
      }
      this.activities = res.datas;
    });
  }
}
