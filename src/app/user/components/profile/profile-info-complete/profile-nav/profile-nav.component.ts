import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProfileService } from '../../../../../services/profile.service';

@Component({
  selector: 'itg-profile-nav',
  templateUrl: 'profile-nav.component.html',
  styleUrls: ['profile-nav.component.scss']
})
export class ProfileNavComponent implements OnInit {
  id: any;
  // user id for POSTing datas
  userData = JSON.parse(localStorage.getItem('currentUser'));
  userId = '';

  constructor (private _profileService: ProfileService, private _activatedRoute: ActivatedRoute) {
    this._activatedRoute.params.subscribe((param: any) => {
      this.id = param ['id'];
    });
  }

  ngOnInit() {
  // user local storage ma cha bhaner set gareko (to solve null exception)
  if (this.userData) {
      this.userId = this.userData.user_id;
    }
  }
}
