import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {ProfileService} from '../../../../../services/profile.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {trigger, style, transition, animate} from '@angular/animations';
import {AuthenticationService} from '../../../../../services/authentication.service';
import {slideAnimation, customLoadingAnimation} from '../../../../../animations';

declare var swal: any;

@Component({
  selector: 'itg-settings',
  templateUrl: 'settings.component.html',
  styleUrls: ['settings.component.scss'],
  animations: [slideAnimation, customLoadingAnimation,
    trigger('slider', [
      transition('void => *', [
        style({
          opacity: 0,
          height: 0
        }),
        animate('0.2s ease-in')
      ]),
      transition('* => void', [
        animate('0.2s ease-out', style({
          opacity: 0,
          height: 0
        }))
      ])
    ]),
  ]
})

export class SettingsComponent implements OnInit, OnDestroy {

  // Variables to find the parent route id
  sub: any;
  parentRouteId: any;

  // Variables for subscribing datas
  profileRes: any;
  projectRes: any;
  experienceRes: any;
  educationRes: any;
  skillsRes: any;

  // Creating the form group
  personalDetailForm: FormGroup;
  experienceForm: FormGroup;
  educationForm: FormGroup;
  skillsForm: FormGroup;

  // boolean to check weather the form is in the DOM or not
  expstatus = false;
  edustatus = false;
  skillstatus = false;

  // boolean to check weather the form is submitted or not
  submitted: boolean;

  // boolean to check if the form is for updating or adding new data
  isUpdatingExp: boolean = false;
  isUpdatingEdu: boolean = false;
  isUpdatingSkill: boolean = false;

  // show all info variable
  allInfo: any;

  // user id for POSTing datas
  userData = JSON.parse(localStorage.getItem('currentUser'));
  userId = '';

  constructor(private _router: Router,
              private route: ActivatedRoute,
              private _profileService: ProfileService,
              private fb: FormBuilder,
              private _authService: AuthenticationService) {
  }

  // Subscribing datas

  showAll(newid) {
    const dummy1 = this._profileService.getUsersById(newid).subscribe(res => {
      this.profileRes = res.users;
    });
    const dummy2 = this._profileService.getExperience(newid).subscribe(res => {
      this.experienceRes = res.experience;
    });
    const dummy3 = this._profileService.getEducation(newid).subscribe(res => {
      this.educationRes = res.datas;
    });
    const dummy4 = this._profileService.getSkills(newid).subscribe(res => {
      this.skillsRes = res.skills;
    });
    setTimeout(() => {
      this.allInfo = [dummy1, dummy2, dummy3, dummy4];
    }, 2000);
  }

  showProfile(id) {
    this._profileService.getUsersById(id).subscribe(res => {
      this.profileRes = res.users;
    });
  }

  showProjects(id) {
    this._profileService.getProjectByUsers(id).subscribe(res => {
      this.projectRes = res.projects;
    });
  }

  showExperience(id) {
    this._profileService.getExperience(id).subscribe(res => {
      this.experienceRes = res.experience;
    });
  }

  showEducation(id) {
    this._profileService.getEducation(id).subscribe(res => {
      this.educationRes = res.datas;
    });
  }

  showSkills(id) {
    this._profileService.getSkills(id).subscribe(res => {
      this.skillsRes = res.skills;
    });
  }

  // createPersonalForm() {
  //     this.personalDetailForm = this.fb.group({
  //         username: ['', Validators.required],
  //         email: ['',  Validators.required],
  //         password: ['',  Validators.required],
  //         website: [''],
  //         //address
  //         country_id:['', Validators.required],
  //         province_id:[''],
  //         zone_id:['', Validators.required],
  //         district_id:['', Validators.required],
  //         city_id:['', Validators.required],
  //         local_address:[''],
  //         //DOB
  //         // date:['',  Validators.required],
  //         //contact
  //         mobile_no:['',  Validators.required],
  //         contact:[''],
  //     });
  // }
  // Subscribing specific data


  // Experience Form

  // function to check weather the add button is clicked or not to display experience form
  expEvent() {
    if (this.expstatus = !this.expstatus) {
      this.expstatus = true;                            // form display hune condition true
      this.isUpdatingExp = false;                       // if is already update active turn it to false
      this.experienceForm.reset();                      // reset the patched values is exists
      this.createExperienceForm();                      // create form to add new experiences
    }
  }

  // function to create exerience form
  createExperienceForm() {
    this.experienceForm = this.fb.group({
      // experience
      company_name: ['', Validators.required],
      position: ['', Validators.required],
      company_location: ['', Validators.required],
      working_status: ['', Validators.required],
      job_description: ['', Validators.required],
    });
    this.experienceForm.patchValue({
      working_status: '1'
    });
  }

  selectedExperienceID: any;                        // variable to set the selected experience id
  selectedExperience: any;                          // variable to set currently selected experience data
  showSpecificExperience(user_id, exp_id) {
    this._profileService.getSpecificExperience(user_id, exp_id).subscribe(res => {
      this.selectedExperience = res.datas;          // assigning the result to selectedExperience
      this.isUpdatingExp = true;                    // to see weather the form is opened for updating or adding new experience
      this.selectedExperienceID = exp_id;           // to pass the selected experience id to service
      this.createExperienceForm();                  // create the experience form
      this.setExpValues();                          // to patch the values of the selected experience to be updated
      this.expstatus = true;                        // to open the form (dropdown of the form)
    });
  }

  // setvalues in form (function to set the selected experience values to be updated to the form)
  setExpValues() {
    this.experienceForm.patchValue({
      // experience
      company_name: this.selectedExperience[0].company_name,
      position: this.selectedExperience[0].title,
      company_location: this.selectedExperience[0].location,
      working_status: this.selectedExperience[0].isworking,
      job_description: this.selectedExperience[0].description,
    });
  }

  onSubmitExp(new_expinfo: any) {
    if (this.experienceForm.valid === true) {             // checking if the form is valid or not
      new_expinfo = this.experienceForm.value;

      if (this.isUpdatingExp === true) {                  // check if the user wants to update or add (update => true)
        this._profileService.updateExperienceInfo(this.userId, this.selectedExperienceID, new_expinfo)
          .subscribe(res => {
            this.showExperience(this.parentRouteId);
          });
      } else {                                            // user wants to add new experience
        this._profileService.addExperienceInfo(this.userId, new_expinfo)
          .subscribe(res => {
            this.showExperience(this.parentRouteId);
          });
      }
      this.submitted = true;                            // set true if the form was sumbitted
      this.expstatus = false;
      this.experienceForm.reset();                      // resetting the values of the form
      this.isUpdatingExp = false;
    } else {
      this.submitted = false;                           // set false if the form is not submitted
    }
  }

  // Education Form
  eduEvent() {
    if (this.edustatus = !this.edustatus) {
      this.edustatus = true;
      this.isUpdatingEdu = false;
      this.createEducationForm();
      this.educationForm.reset();
    }
  }

  // using form builder to createform
  createEducationForm() {
    this.educationForm = this.fb.group({
      // education
      institute: ['', Validators.required],
      degree: ['', Validators.required],
      major_subject: ['', Validators.required],
      grades: ['', Validators.required],
      description: ['', Validators.required],
    });
  }

  selectedEducationId: any;
  selectedEducation: any;

  showSpecificEducation(user_id, edu_id) {
    this._profileService.getSpecificEducation(user_id, edu_id).subscribe(res => {
      this.selectedEducation = res.datas;
      this.isUpdatingEdu = true;
      this.selectedEducationId = edu_id;
      this.createEducationForm();
      this.setEduValues();
      this.edustatus = true;
    });
  }

  // setting selected education details to form
  setEduValues() {
    this.educationForm.patchValue({
      institute: this.selectedEducation[0].institution,
      degree: this.selectedEducation[0].degree_level,
      major_subject: this.selectedEducation[0].major_study_area,
      grades: this.selectedEducation[0].grades,
      description: this.selectedEducation[0].description
    });
  }

  // onsubmit method
  onSubmitEdu(new_eduinfo: any) {
    if (this.educationForm.valid === true) {
      new_eduinfo = this.educationForm.value;
      if (this.isUpdatingEdu === true) {
        this._profileService.updateEducationInfo(this.userId, this.selectedEducationId, new_eduinfo)
          .subscribe(res => {
            this.showEducation(this.parentRouteId);
          });
        // swal('Congratulation', 'Your form has been Updated', 'success');
      } else {
        this._profileService.addEducationInfo(this.userId, new_eduinfo)
          .subscribe(res => {
              this.showEducation(this.parentRouteId);
            },
            error => {
              console.log(error);
            });

        swal('Congratulation', 'Your form has been Saved', 'success');
      }
      this.submitted = true;
      this.educationForm.reset();
    } else {
      this.submitted = false;
    }
  }


  // Skills form
  skillsEvent() {
    if (this.skillstatus = !this.skillstatus) {
      this.skillstatus = true;
      this.createSkillsForm();
    }
  }

  createSkillsForm() {
    this.skillsForm = this.fb.group({
      // skills
      skill: ['', Validators.required],
    });
  }

  onSubmitSkill(new_skillinfo: any) {
    if (this.skillsForm.valid === true) {
      new_skillinfo = this.skillsForm.value;
      this._profileService.addSkillsInfo(this.userId, new_skillinfo)
        .subscribe(res => {
          this.showSkills(this.parentRouteId);
        });
      this.submitted = true;
      this.skillsForm.reset();
      this.skillstatus = false;
    } else {
      this.submitted = false;
    }
  }


  ngOnInit() {
    this.sub = this.route.parent.params.subscribe(params => {
      this.parentRouteId = +params['id'];
    });
    if (this.userData) {
      this.userId = this.userData.user_id;
    }
    // this.showProfile(this.parentRouteId);
    // this.showProjects(this.parentRouteId);
    // this.showEducation(this.parentRouteId);
    // this.showExperience(this.parentRouteId);
    // this.showSkills(this.parentRouteId);
    this.showAll(this.parentRouteId);
    this.createExperienceForm();

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
