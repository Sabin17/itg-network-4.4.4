import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../services/authentication.service';
import {Router} from '@angular/router';

import { LocalStorageService } from '../../../services/localStorage.service';

@Component({
  selector: 'itg-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.scss'],
})
export class LoginComponent implements OnInit{
  authUser: any = {};
  error = '';
  loading: boolean = false;

  constructor(private router: Router,
              private _authService: AuthenticationService,
              private _localStorageService: LocalStorageService
  ) {}

  userlogin(){
    this.loading = true;
    this._authService.login(this.authUser.email, this.authUser.password)
      .subscribe(
        result => {
          if(result === true){
            this.announce();
            this.router.navigate(['/dashboard']);
            // window.location.reload();
            // this.router.navigate(['/dashboard']);
          }else{
            this.loading = false;
            this.error = 'Username or Password Do not Match';
          }
        });
  }

  public announce(){
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser && currentUser.token){
      this._localStorageService.announceLogin(currentUser);
    }
  }

  ngOnInit(){

  }

}
