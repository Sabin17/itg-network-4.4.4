import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup , Validators} from '@angular/forms';
import { RegisterService } from '../../../services/register.service';
import {Router} from '@angular/router';

declare var swal: any;

@Component({
  selector: 'itg-register',
  templateUrl: 'dummy.html',
  styleUrls: ['register.component.scss'],
})

export class RegisterComponent implements OnInit {

    registerForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private _registerService: RegisterService,
        private router: Router
    ) {
        // this.userForm();
        // this.showCourses();
    }

    userForm() {
    this.registerForm = this.fb.group({
        fname: ['', Validators.required],
        mname: '',
        lname: ['', Validators.required],

        gender: ['', Validators.required],

        email: ['', [Validators.required]],
        contact: [''],
        mobile_no: ['', Validators.required],
        course_id: ['', Validators.required],
        whyus: [''],
    });
    this.registerForm.patchValue({
      gender: 'male'
    });
   }

    // onsubmit method
    submitted: boolean;
    onSubmit(newuser: any) {
        if (this.registerForm.valid === true) {
            newuser = this.registerForm.value;
            // this._registerService.registerUser(newuser)
            //     .subscribe(res => console.log(res));
            this.submitted = true;
            this.registerForm.reset();
            this.router.navigate(['/dashboard']);
        }else {
            this.submitted = false;
        }
    }

    openAlert() {
        if (this.registerForm.valid === true) {
            swal('Congratulation', 'Your form has been Submitted', 'success');
        }else {
            swal('Sorry!!', 'Your form is Invalid', 'error');
        }
    }

    // course language
    courses: any;

    showCourses() {
      this._registerService.getCourses().subscribe(res => {
        this.courses = res.datas;
      });
    }

    ngOnInit() {

    }
}
