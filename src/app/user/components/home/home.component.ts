import {Component, OnInit} from '@angular/core';
import {MembersService} from '../../../services/members.service';
import {PostService} from '../../../services/post.service';


@Component({
  selector: 'itg-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.scss'],
})
export class HomeComponent implements OnInit {
  // members section
  members: any;
  profile_img: any;
  // forum section
  forums: any;

  constructor(private _membersService: MembersService, private _postService: PostService) {
    this.showMembers();
    this.getAppPosts();
  }

  // members
  showMembers() {
    this._membersService.getMembers().subscribe(res => {
      const founders = res.founders;
      const mentors = res.mentors;
      const submentors = res.submentors;
      const membersonly = res.membersonly;
      this.profile_img = res.member_profile;
      this.members = founders.concat(mentors).concat(submentors).concat(membersonly);
    });
  }

  // forum section
  getAppPosts() {
    this._postService.getAllPosts().subscribe(result => {
      this.forums = result.posts;
    });
  }

  ngOnInit() {
  }

}
