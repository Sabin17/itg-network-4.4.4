import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormControl, FormGroup , Validators} from '@angular/forms';
import { PostService} from '../../../../services/post.service';
import { Forum } from '../forum-post/forum.model';
import {SearchService} from '../../../../services/search.service';
import {customLoadingAnimation, sectionAnimation} from '../../../../animations/slide.animation';


@Component({
  selector: 'itg-forum-list',
  templateUrl: 'forum-list.component.html',
  styleUrls: ['forum-list.component.scss'],
  animations: [sectionAnimation, customLoadingAnimation]
})
export class ForumListComponent implements OnInit {

forumObj: any = {};
resposts: any;
resmessage: any;
currentUser: any;
userData = JSON.parse(localStorage.getItem('currentUser'));
userId = '';

  constructor( private fb: FormBuilder,
               private _postService: PostService,
               private route: ActivatedRoute,
               private _searchService: SearchService) {
    // this.getAppPosts();
    const userdata = localStorage.getItem('currentUser');
  }

  getAppPosts() {
    this._postService.getAllPosts().subscribe(result => {
        this.resposts = result.posts;
        this.resmessage = result.message;
    });
  }

  forumForm: FormGroup;

  submitPost(Value: Forum) {
      const forumPostData = this.forumForm.value;
      this._postService.createForumPost(forumPostData).subscribe( res =>
        console.log('')
      );
  }

  forumPostForm() {
    // user local storage (to solve null exception)
    this.userId = this.userData.user_id;

    this.forumForm = this.fb.group({
        post_title: ['', Validators.required],
        post_body: ['', Validators.required],
        post_image: [''],
        category_id: ['', Validators.required],
        user_id: [this.userId],
     });
   }

  ngOnInit() {
    this.getAllForumCategories();

     this.route.params.subscribe((params) => {
        this.cat_id = +params['cat_id'];

        if (this.cat_id) {
          this.getForumAccordingToCategory(this.cat_id);
        }else {
          this.getAppPosts();
        }
     });
  }

  rescategories: any;
  message: string;

  getAllForumCategories() {
    this._postService.getAllCategories().subscribe(result => {
        this.message = result.message;
        this.rescategories = result.categories;
    });
  }

  // for all post which shuld be listed from forum category
  cat_id: any;

  getForumAccordingToCategory(cat_id: any) {
      this._postService.getForumPostByCatefory(cat_id).subscribe(result => {
         this.resposts = result.posts;
         this.message = result.message;
      });
  }

  searchtext: any;
  searchForum() {
    if (this.searchtext.length === 0) {
      this._postService.getAllPosts();
    }else {
      this._searchService.updateForum(this.searchtext);
      this._searchService.searchForum().subscribe(res => {
        this.resposts = res.datas;
      });
    }
  }

  ngOnDestroy() {

  }
}
