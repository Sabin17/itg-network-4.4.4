import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AnonymousSubscription } from 'rxjs/Subscription';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { PostService } from '../../../../services/post.service';
import {AuthenticationService} from "../../../../services/authentication.service";
declare var swal: any;

@Component({
  selector: 'itg-forum-detail',
  templateUrl: 'forum-detail.component.html',
  styleUrls: ['forum-detail.component.scss']
})
export class ForumDetailComponent implements OnInit {
  code = `export class SafeHtml {
      constructor(private sanitizer:Sanitizer){}

      transform(html) {
        return this.sanitizer.bypassSecurityTrustHtml(html);
      }
    } `;

// code: any;
  id: any;

//for real time changing
  private timerSubscription: AnonymousSubscription;
  private commentsSubscription: AnonymousSubscription;

  resposts: any;
  rescomments: any;
  message: any;
  countComment: any;
// changeComment: EventEm
  userData = JSON.parse(localStorage.getItem('currentUser'));
  userId = '';

//variable for allcomment
  commentForumForm: FormGroup;

  constructor(
    private _postService: PostService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private _authService: AuthenticationService
  ) {

  }

  ngOnInit() {
    if (this.userData) {
      this.userId = this.userData.user_id;
    }
    this.CommentForm();

// this code will get the particular post id from the selection
    this.route.params.subscribe((params) => {
      this.id = params['id'];
      this._postService.getSelectedPostsById(this.id).subscribe(result =>{
        this.resposts = result.post;
        this.code = this.resposts.post_body;
        this.getForumComment();
        //  this.rescomments = result.allcomment;
        //  this.message = result.message;
      });
    });

    this.getForumComment();
  }

  CommentForm(){
    this.commentForumForm = this.fb.group({
      comment_title: ['', Validators.required],
      comment_body: ['', Validators.required],
      user_id: [this.userId],
      post_id: [this.id]
    });
  }

  postForumComment(comments: string) {
    this._postService.createForumComment(this.commentForumForm.value, this.id).subscribe( result => {
      this.getForumComment();

      swal(
        'Success',
        'Your Comment is Recorded',
        'success'
      );

      this.CommentForm();
      // this.commentForumForm.reset();
    })
  }

  getForumComment(): void{
    this._postService.getCommentOfForum(this.id).subscribe( result => {
      this.rescomments = result.allcomment;
      this.countComment = this.rescomments.length;
      // console.log(this.rescomments);
      // this.subscribeToComment();
    });
    if (this.userData) {
      this.userId = this.userData.user_id;
    }
  }

  // private subscribeToComment(): void{
  //   this.timerSubscription = Observable.timer(5000).first().subscribe(() => this.getForumComment());
  // }

  public ngOnDestroy(): void {
    if (this.timerSubscription) {
      this.timerSubscription.unsubscribe();
    }
    if (this.commentsSubscription) {
      this.commentsSubscription.unsubscribe();
    }
  }
}
