import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from '../../../../services/post.service';
import { AuthenticationService } from '../../../../services/authentication.service';


@Component({
  selector: 'itg-forum-filter-list',
  templateUrl: 'forum-filter-list.component.html',
  styleUrls: ['forum-filter-list.component.css']
})
export class ForumFilterListComponent implements OnInit {


//variable declearation
message: any;
rescategories: any;
isLoggedIn: boolean = false;

  constructor(private _postService: PostService, private route: ActivatedRoute, private _authService: AuthenticationService) {
      this.getAllForumCategories();

      if(this._authService.token != null){
        this.isLoggedIn = true;
      }
   }

  ngOnInit() {
    // this.route.params.subscribe((params) => {
    //   const id = params['id'];

    //   this._postService.getForumPostAccToCategory(id).subscribe(result =>{
    //   });
    // })
  }

  getAllForumCategories() {
    this._postService.getAllCategories().subscribe(result => {
        this.message = result.message;
        this.rescategories = result.categories;
    });
  }

  showConfirmdialog() {
  }

}
