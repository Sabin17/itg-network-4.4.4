import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ForumListComponent} from './forum-list/forum-list.component';
import {ForumPostComponent} from './forum-post/forum-post.component';
import {ForumDetailComponent} from './forum-detail/forum-detail.component';

export const forumRoutes: Routes = [
  {
    path: '',
    component: ForumListComponent
  },
  {
    path: 'forum-post',
    component: ForumPostComponent
  },
  {
    path: 'forum-detail/:id/:post_title',
    component: ForumDetailComponent
  },
  {
    path: 'forum/category/:cat_id/:cat_name',
    component: ForumListComponent
  },

];
