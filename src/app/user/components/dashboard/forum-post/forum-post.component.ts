import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {PostService} from '../../../../services/post.service';
import {AuthenticationService} from '../../../../services/authentication.service';

declare const swal: any;

@Component({
  selector: 'itg-forum-post',
  templateUrl: 'forum-post.component.html',
  styleUrls: ['forum-post.component.scss']
})
export class ForumPostComponent implements OnInit {

  forumObj: any = {};
  isLoggedIn: boolean = false;
  userData = JSON.parse(localStorage.getItem('currentUser'));
  userId = this.userData.user_id;
  forumForm: FormGroup;

  constructor(private fb: FormBuilder,
              private _postService: PostService,
              private _authService: AuthenticationService,
              private router: Router) {
    if (this._authService.token != null) {
      this.isLoggedIn = true;
    }
  }

  forumPostForm() {
    this.forumForm = this.fb.group({
      post_title: ['', Validators.required],
      post_body: ['', Validators.required],
      post_image: [''],
      category_id: ['', Validators.required],
      user_id: [this.userId],
    });
  }


  submitPost(Value) {
    const forumPostData = this.forumForm.value;
    this._postService.createForumPost(forumPostData).subscribe(res =>
      console.log('')
    );
    swal('', 'Problem Posted', 'success');
    this.forumForm.reset();
    this.router.navigate(['/dashboard']);
  }

  ngOnInit() {
    this.getAllForumCategories();
    this.forumPostForm();
  }

  rescategories: any;
  message: string;

  getAllForumCategories() {
    this._postService.getAllCategories().subscribe(result => {
      this.message = result.message;
      this.rescategories = result.categories;
    });
  }
}
