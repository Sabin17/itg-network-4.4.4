export class Forum {
  id?: number;
  post_title: string;
  post_body: string;
  post_image: string;
}
