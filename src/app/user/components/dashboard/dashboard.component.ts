import { Component, OnInit} from '@angular/core';
import {PostService} from '../../../services/post.service';
import {SearchService} from '../../../services/search.service';
import {Http} from '@angular/http';

@Component({
  selector: 'itg-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.css'],
})
export class DashboardComponent implements OnInit {

  constructor( private http: Http, private _postService: PostService, private _searchService: SearchService) { }

  ngOnInit() {
  }


}
