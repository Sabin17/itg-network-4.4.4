import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { AuthenticationService } from '../../../services/authentication.service';
import { LocalStorageService } from '../../../services/localStorage.service';
// declare var swal:any;

@Component({
  selector: 'itg-navigation',
  templateUrl: 'navigation.component.html',
  styleUrls: ['navigation.component.scss'],

})
export class NavigationComponent implements OnInit {
  // private isLoggedIn: boolean = false;
  // private loginValue: boolean = true; //if user logged in cha vane chai
  subscription: Subscription;

  isLoggedIn: boolean = false;
  userData: any = '';
  userEmail: string = '';
  userName: string = '';
  userId: any;

  isActive: boolean;
  currentUser: any;
  constructor(private _authService: AuthenticationService,
              private router: Router,
              private _localStorageService: LocalStorageService
  ) {

    this.subscription = _localStorageService.loginAnnounced$.subscribe(
      currentUser => {
        this.currentUser = currentUser;
      });

    this.subscription = _localStorageService.logoutAnnounced$.subscribe(
      empty => {
        this.currentUser = null;
      });

    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));

    // if(this._authService.token){
    //   this.isLoggedIn = true;
    // }else{
    //   this.router.navigate(['/home']);
    // }
  }


  userLogout() {
    this._authService.logout();
    this.announce();
    this.router.navigate(['/login']);
  }

  public announce() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (!currentUser) {
      this._localStorageService.announceLogout();
    }
  }

  ngOnInit() {
    if (localStorage.getItem('currentUser')) {
      this.isLoggedIn = true;
      this.userData = JSON.parse(localStorage.getItem('currentUser'));
      this.userEmail = this.userData.email;

      this.userId = this.userData.user_id;
      this.userName = this.userData.username;
    }
  }
}
