import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostService } from '../../../../services/post.service';

@Component({
  selector: 'itg-event-detail',
  templateUrl: 'event-detail.component.html',
  styleUrls: ['event-detail.component.css']
})
export class EventDetailComponent implements OnInit {

name: string;
  constructor(
    private route: ActivatedRoute,
    private _postService: PostService
  ) {
    this.name = 'aashish';
    this.route.params.subscribe((params) => {
      const id = params['id'];
      this.getSingleEvent(id);
    });
  }
  getSingleEvent(id) {
    this._postService.getSpecificEventById(id).subscribe(result => {
    });
  }

  ngOnInit() {
  }

}
