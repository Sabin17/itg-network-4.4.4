import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventComponent } from './event.component';
import { EventDetailComponent } from './event-detail/event-detail.component';

export const eventRoutes: Routes = [
  {
      path: '',
      component: EventComponent
  },
  {
      path: '/event-detail/:id',
      component: EventDetailComponent
  },

];