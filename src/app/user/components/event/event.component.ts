import { Component, OnInit } from '@angular/core';
import { PostService } from '../../../services/post.service';
import {SearchService} from '../../../services/search.service';
import {customLoadingAnimation, sectionAnimation} from '../../../animations/slide.animation';

@Component({
  selector: 'itg-event',
  templateUrl: 'event.component.html',
  styleUrls: ['event.component.scss'],
  animations: [sectionAnimation, customLoadingAnimation]
})
export class EventComponent implements OnInit {

  events: any;
  message: any;
  eventname: any;

  constructor( private _postService: PostService, private _searchService: SearchService) {
    setTimeout(() => {
      this.getAllEvents();
    }, 3000);
  }

  getAllEvents() {
    this._postService.getAllEventPosts().subscribe(res => {
      this.events = res.events;
      this.message = res.message;
    });
  }

  searchEvents() {
    if (this.eventname.length === 0) {
      this.getAllEvents();
    }else {
      this._searchService.updateEvent(this.eventname);
      this._searchService.searchEvents().subscribe(res => {
        this.events = res.datas;
      });
    }
  }

  ngOnInit() {
  }

}
