import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';

@Injectable()
export class AdminService {

    private base_url = 'http://api.itglance.org/api/admin/';
    constructor(private http: Http) { }

    getUnverifiedUsers() {
        return this.http.get(this.base_url + 'users').map(res => res.json());
    }

    verifyUserById(id: number) {
        return this.http.get(this.base_url + 'verify/user/' + id).map(
                res => res.json()
        );
    }

    getUnverifiedPosts() {
        return this.http.get(this.base_url + 'pending/forum/posts').map(res => res.json());
    }

    verifyPostById(id: number) {
        return this.http.get(this.base_url + 'verify/forum/post/' + id).map(
                res => res.json()
        );
    }

    getAllAdminAnalytics() {
        return this.http.get(this.base_url + 'analytics').map(res => res.json());
    }

    createEventPost(event: any) {
        const headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http.post(this.base_url + 'event/create', JSON.stringify(event), {headers: headers})
        .map(
            res => res.json()
        );
    }
}
