import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class SearchService {

    base_url = 'http://api.itglance.org/api/search/';
    private name: string;

    constructor(private http: Http) {
      console.log('search service started');
      this.name = '';
    }

    updateUser(user: string) {
      this.name = user;
    }
    public searchMembers() {
       return this.http.get(this.base_url + 'user/' + this.name)
         .map (res => res.json());
    }

    updateEvent(event: string) {
      this.name = event;
    }
    public searchEvents() {
      return this.http.get(this.base_url + 'event/' + this.name)
        .map (res => res.json());
    }

    updateForum(forum: string) {
      this.name = forum;
    }
    public searchForum() {
      return this.http.get(this.base_url + 'forum/' + this.name)
        .map (res => res.json());
    }
}
