import {Injectable} from '@angular/core';
import {Http, Headers, Response, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Rx';
import {AuthenticationService} from './authentication.service';


@Injectable()

export class ProfileService {

  base_url = 'http://api.itglance.org/api/';

  constructor(private http: Http, private _authService: AuthenticationService) {
    console.log('profile service started');
  }

  // getting personal profile (Login bhako user ko lagi)
  public getUsersById(id) {
    return this.http.get(this.base_url + 'users/' + id + '?token=' + this._authService.token)
      .map(response => response.json());
  }

  public getProjectByUsers(id) {
    return this.http.get(this.base_url + 'users/' + id + '/projects?token=' + this._authService.token)
      .map(response => response.json());
  }


  // For adding profile details (Login User)
  // <<------------------------------------POST Methods>-------------------------------------->>

  public addEducationInfo(id, eduinfo: any) {
    const body = JSON.stringify(eduinfo);
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    return this.http.post(this.base_url + 'users/' + id + '/education/create', body, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  public addExperienceInfo(id, expinfo: any) {
    const body = JSON.stringify(expinfo);
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    return this.http.post(this.base_url + 'users/' + id + '/experience/create', body, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  public addSkillsInfo(id, skillsinfo: any[]) {
    const body = JSON.stringify(skillsinfo);
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    return this.http.post(this.base_url + 'users/' + id + '/skill/create', body, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }


  // for updating profile details

  // getting specific experience details
  public getSpecificExperience(user_id, exp_id) {
    return this.http.get(this.base_url + 'users/' + user_id + '/experience/' + exp_id)
      .map(res => res.json());
  }

  // getting specific education details
  public getSpecificEducation(user_id, edu_id) {
    return this.http.get(this.base_url + 'users/' + user_id + '/education/' + edu_id)
      .map(res => res.json());
  }

  // getting specific skills details
  public getSpecificSkills(user_id, skill_id) {
    return this.http.get(this.base_url + 'users/' + user_id + '/skills/' + skill_id)
      .map(res => res.json());
  }

  //  <<------------------------------------ PUT Methods -------------------------------------->>

  // Updating personal details
  public updatePersonalInfo(id, personalinfo: any) {
    const body = JSON.stringify(personalinfo);
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    return this.http.put(this.base_url + '//api end point', body, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  // Updating experience details
  public updateExperienceInfo(id, exp_id, expinfo: any) {
    const body = JSON.stringify(expinfo);
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    return this.http.put(this.base_url + 'users/' + id + '/experience/' + exp_id + '/update', body, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  // Updating education details
  public updateEducationInfo(id, edu_id, eduinfo: any) {
    const body = JSON.stringify(eduinfo);
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    return this.http.put(this.base_url + 'users/' + id + '/education/' + edu_id + '/update', body, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  // Updating skills details
  public updateSkillsInfo(id, skillsinfo: any[]) {
    const body = JSON.stringify(skillsinfo);
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});
    return this.http.put(this.base_url + 'users/' + id + '/skill/create', body, options)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }


  // view others profile and details (Guest User ko Lagi)
  public getOthersProfile(id) {
    return this.http.get(this.base_url + 'users/member/' + id)
      .map(response => response.json());
  }

  public getOthersProject(id) {
    return this.http.get(this.base_url + 'users/' + id + '/projects')
      .map(response => response.json());
  }

  public getSkills(id) {
    return this.http.get(this.base_url + 'users/' + id + '/skills')
      .map(response => response.json());
  }

  public getExperience(id) {
    return this.http.get(this.base_url + 'users/' + id + '/experience/detail')
      .map(response => response.json());
  }

  public getEducation(id) {
    return this.http.get(this.base_url + 'users/' + id + '/education/detail')
      .map(response => response.json());
  }

  public getAllActivitiesOfUser(id) {
    return this.http.get(this.base_url + 'users/' + id + '/activity/lists')
      .map(response => response.json());
  }
}
