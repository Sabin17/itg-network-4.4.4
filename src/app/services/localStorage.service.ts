import { Injectable, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class LocalStorageService {

    private missionAnnouncedSource = new Subject<string>();
    private logoutAnnouncedSource = new Subject<string>();

    loginAnnounced$ = this.missionAnnouncedSource.asObservable();
    logoutAnnounced$ = this.logoutAnnouncedSource.asObservable();

    announceLogin(mission: string){
        //if localstorage ma current user save vako cha vane
        this.missionAnnouncedSource.next(mission);
    }

    announceLogout(){
        //id localstorage bata data remove vako cha vane
        this.logoutAnnouncedSource.next(null);
    }
    constructor() { }
}