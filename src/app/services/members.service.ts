import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()

export class MembersService {
    base_url = 'http://api.itglance.org/api/';

    constructor(private http: Http) {
      console.log('profile service started');
    }

    public getMembers() {
      return this.http.get(this.base_url + 'members')
        .map(response => response.json());
    }
}
