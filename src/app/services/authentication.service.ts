import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthenticationService {
    // this is for storing the token if user is authenticated variable
    public token: string;
    public user_id: number;
    public username: any;

    base_url = 'http://api.itglance.org/api/';

    constructor(private http: Http) {
        // set token if saved in local storage
        var currentUser = JSON.parse(
            localStorage.getItem('currentUser')
        );
        this.token = currentUser && currentUser.token;
    }
    // this function is type of observable and will return boolean value
    // true is user is authenticated or false for vice versa
    login(email: string, password: string): Observable<boolean> {

        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        // headers.append('', 'http://localhost:8008/api/');
        // headers.append('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

        return this.http.post(this.base_url + 'users/signin',
        JSON.stringify({email: email, password: password}),
        {headers: headers}
        )
        .map((response: Response) => {
                // login successful if there is a jwt token in the response
                // so if token return aayo vane we can be sure that the user is authenticated
                let token = response.json && response.json().token;
                let user_id = response.json && response.json().user_id;
                let username = response.json && response.json().username;

                if (token) {
                    // if token aako cha vane new token set garne
                    // set token property
                    this.token = token;

                    // now store the user in local storage so that current user can be accessed between different pages
                    localStorage.setItem(
                        'currentUser', JSON.stringify({
                            email: email,
                            token: token,
                            user_id: user_id,
                            username: username

                    }));
                    // here we can actually use localstorage directly
                    // this is provided by browser itself and there alternatives to store or set the values in localstorage like
                    // 1> localStorage.currentUser = 'user data',
                    // 2> localStroage['currentUser'] = 'user ko data'
                    // 3> localStroage.setItem('currentUser', 'user ko data')

                    // return true to indicate successful login

                    return true;
                }else {
                    // return false to indicate failed login
                    return false;
                }
            }
        );
    }


    // function for user logout
    logout(): void {
        // clear token and remove the user from the local storage to logout the user
        this.token = null;
        localStorage.removeItem('currentUser');
    }
}
