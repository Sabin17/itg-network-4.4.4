import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { AuthenticationService } from './authentication.service';
import { Forum } from './../user/components/dashboard/forum-post/forum.model';

@Injectable()
export class PostService {
  headers: Headers;

  base_url = 'http://api.itglance.org/api/';

  constructor(private http: Http, private _authService: AuthenticationService) {
    this.headers = new Headers();
    this.headers.append('Content-Type', 'multipart/form-data');
    this.headers.append('Accept', 'application/json');
  }

// this function will get all the forum posts available in the database which are active and are listed according to the date
  getAllPosts(){
    // return this.http.get(this.base_url+'forum?token='+this._authService.token)
    return this.http.get(this.base_url+'forum')
      .map(res => res.json()
      );
  }

// this function is to get a specific selected forum post and will get all the related info about this single forum according tot he id
  getSelectedPostsById(id: any){
    return this.http.get(this.base_url+'forum/'+id)
      .map(res => res.json());
  }

//function to get forum list according to category
  getForumPostByCatefory(cat_id: any){
    return this.http.get(this.base_url+'forum/category/'+cat_id)
      .map(
        res => res.json()
      )
  }

  //this is the function to get all the available categories in our application
  getAllCategories(){
    return this.http.get(this.base_url+'categories')
      .map(res => res.json());
  }

  getForumPostAccToCategory(id: any){
    return this.http.get(this.base_url+'forum/category/'+id)
      .map(res => res.json());
  }

  getAllEventPosts(){
    return this.http.get(this.base_url+'events')
      .map(
        res => res.json()
      );
  }

  getSpecificEventById(id: any){
    return this.http.get(this.base_url+'events/'+id)
      .map(
        res => res.json()
      )
  }

  createForumPost(forum: any){
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.base_url+'forum/post', JSON.stringify(forum), {headers:headers})
      .map(
        res => res.json()
      )
  }

  createForumComment(datas: any, id: any){
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(this.base_url+'forum/'+id+'/comment/create',
      JSON.stringify(datas), {headers: headers})
      .map(res =>
        res.json()
      );
  }

  getCommentOfForum(id: any) {
    return this.http.get(this.base_url + 'forum/'+id+'/comment/list')
      .map(res =>
        res.json()
      );
  }

  createFeedbackPost(feedback: any) {
    // console.log(photo)
    const headers = new Headers();
    headers.append('Content-Type', 'multipart/form-data');
    return this.http.post(this.base_url + 'feedback/create', JSON.stringify(feedback), { headers: headers})
      .map(
        res => res.json()
      );
  }

  getAllFeedbacks() {
    return this.http.get(this.base_url + 'feedback/list')
      .map(
        res => res.json()
      );
  }
}
