import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable} from 'rxjs/Rx';
import { User } from '../user/interface/user.interface';

@Injectable()

export class RegisterService {

    base_url = 'http://api.itglance.org/api/';

    constructor(private http: Http) {
      console.log('register service started');
    }

    // Register a new user
    public registerUser (user: User) {
        const body = JSON.stringify(user); // Stringify payload
        const headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
        const options = new RequestOptions({ headers: headers }); // Create a request option
        return this.http.post(this.base_url + 'users/register', body , options) // ...using post request
                         .map((res: Response) => res.json()) // ...and calling .json() on the response to return data
                         .catch((error: any) => Observable.throw(error.json().error || 'Server error'));  // ...errors if any
    }

    public getCountry() {
        return this.http.get(this.base_url + 'address/country')
            .map(res => res.json());
    }
    public getProvince(id) {
        return this.http.get(this.base_url + 'address/province/' + id)
            .map(res => res.json());
    }
    public getZone(id) {
        return this.http.get(this.base_url + 'address/zone/' + id)
            .map(res => res.json());
    }
    public getDistrict(id) {
        return this.http.get(this.base_url + 'address/district/' + id)
            .map(res => res.json());
    }
    public getCity(id) {
        return this.http.get(this.base_url + 'address/city/' + id)
            .map(res => res.json());
    }

    public getCourses() {
      return this.http.get(this.base_url + 'language')
        .map(res => res.json());
    }
}
