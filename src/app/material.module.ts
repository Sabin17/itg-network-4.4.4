import {
  MdChipsModule,
  MdButtonModule,
  MdCheckboxModule,
  MdIconModule,
  MdProgressBarModule,
  MdCardModule,
  MdTooltipModule,
  MdSelectModule,
  MdMenuModule,
  MdSlideToggleModule,
  MdSnackBarModule,

} from '@angular/material';
import {NgModule} from '@angular/core';


@NgModule({
  imports: [
    MdChipsModule,
    MdButtonModule,
    MdCheckboxModule,
    MdIconModule,
    MdProgressBarModule,
    MdCardModule,
    MdTooltipModule,
    MdSelectModule,
    MdMenuModule,
    MdSlideToggleModule,
    MdSnackBarModule,

  ],
  exports: [
    MdChipsModule,
    MdButtonModule,
    MdCheckboxModule,
    MdIconModule,
    MdProgressBarModule,
    MdCardModule,
    MdTooltipModule,
    MdSelectModule,
    MdMenuModule,
    MdSlideToggleModule,
    MdSnackBarModule,

  ],
})
export class ItgMaterialModule {}
