import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {LoginComponent} from './user/components/login/login.component';
import {ProfileComponent} from './user/components/profile/profile.component';
import {HomeComponent} from './user/components/home/home.component';
import {DashboardComponent} from './user/components/dashboard/dashboard.component';
import {RegisterComponent} from './user/components/register/register.component';

import {PROFILE_INFO_ROUTES} from './user/components/profile/profile-info-complete/profile-info.routes';
import {MembersComponent} from './user/components/members/members.component';
import {forumRoutes} from './user/components/dashboard/forum.routing';
import {EventComponent} from './user/components/event/event.component';
import {FeedbackComponent} from './user/components/feedback/feedback.component';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'profile',
    children: [
      {
        path: ':id',
        component: ProfileComponent,
        children: PROFILE_INFO_ROUTES
      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'members',
    component: MembersComponent,
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    children: forumRoutes
  },
  {
    path: 'event',
    component: EventComponent,
  },
  {
    path: 'feedback',
    component: FeedbackComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: 'admin',
    loadChildren: 'app/admin/admin.module#AdminModule',
    // component: AdminDashboardComponent,
    // children: adminRouting,
  }
];

export const appRoutingModule: ModuleWithProviders = RouterModule.forRoot(appRoutes, {useHash: true});
