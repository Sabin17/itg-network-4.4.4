import {trigger, state, style, transition, animate, group} from '@angular/animations';

export const scaleInAnimation =
  trigger('scaleIn', [
    transition('void => *', [
      style({transform: 'scale(0)', opacity: 0, 'transform-origin': 'left center'}),
      animate('.3s .1s ease-in', style({transform: 'scale(1)', opacity: 1}))
    ]),
  ]);
