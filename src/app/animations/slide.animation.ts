import {trigger, style, transition, animate, query, stagger} from '@angular/animations';

export const slideAnimation =
  trigger('slide', [
    transition('void => *', [
      style({display: 'none', transform: 'translateY(50px)', opacity: 0, 'trasform-origin': 'center center'}),
      animate('300ms .3s ease', style({transform: 'translateY(0)', opacity: 1})),
    ])
  ]);

export const sectionAnimation =
  trigger('sectionAnim', [
    transition('* => *', [
      query('section', style({ opacity: 0, transform: 'translateY(-40px)' }), {optional: true}),

      query('section', stagger('100ms', [
        animate('300ms .3s ease-out', style({ opacity: 1, transform: 'translateY(0)' })),
      ]), {optional: true}),
    ])
  ]);


export const customLoadingAnimation =
  trigger('loadingAnim', [
    transition('void => *', [
      style({display: 'none', transform: 'translateY(20%)', opacity: 0, 'trasform-origin': 'center center'}),
      animate(300, style({transform: 'translateY(0)', opacity: 1})),
    ]),
    // transition('* => void', [
    //   animate(300, style({opacity: 0, display: 'none'})),
    // ])
  ]);

