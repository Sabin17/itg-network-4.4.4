import {trigger, style, transition, animate, stagger, query} from '@angular/animations';

export const fadeAnimation =
  trigger('fadeAnim', [
    transition('* => *', [
      query(':self', style({ opacity: 0, transform: 'scale(.8)'}), {optional: true}),

      query(':self', stagger('200ms', [
        animate('2000ms .2s ease-out', style({ opacity: 1, transform: 'scale(1)' })),
      ]), {optional: true}),
    ])
  ]);
