import {trigger, style, transition, keyframes, animate, stagger, query} from '@angular/animations';

export const slideDownAnimation =
  trigger('listSlideDownAnim', [
    transition('* => *', [
      query(':enter', style({ opacity: 0 }), {optional: true}),
      query(':enter', stagger('300ms', [
        animate('1s ease-in', keyframes([
          style({opacity: 0, transform: 'translateY(-75%)', offset: 0}),
          style({opacity: .5, transform: 'translateY(35px)', offset: 0.3}),
          style({opacity: 1, transform: 'translateY(0)', offset: 1.0}),
        ]))]), {optional: true}),
      query(':leave', stagger('300ms', [
        animate('1s ease-in', keyframes([
          style({opacity: 1, transform: 'translateY(0)', offset: 0}),
          style({opacity: .5, transform: 'translateY(35px)', offset: 0.3}),
          style({opacity: 0, transform: 'translateY(-75%)', offset: 1.0}),
        ]))]), {optional: true})
    ])
  ]);

export const slideRightAnimation =
  trigger('listSlideRightAnim', [
    transition('* => *', [
      query(':enter', style({ opacity: 0, transform: 'translateX(-40px)' }), {optional: true}),

      query(':enter', stagger('500ms', [
        animate('800ms 1.2s ease-out', style({ opacity: 1, transform: 'translateX(0)' })),
      ]), {optional: true}),
    ])
  ]);
