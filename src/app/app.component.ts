import {AfterViewInit, Component} from '@angular/core';
import {ProfileService} from './services/profile.service';
import {PostService} from './services/post.service';
import {MembersService} from './services/members.service';
import {AuthenticationService} from './services/authentication.service';
import {RegisterService} from './services/register.service';
import {SearchService} from './services/search.service';
import {LocalStorageService} from './services/localStorage.service';
import {NavigationCancel, NavigationEnd, NavigationStart, Router} from '@angular/router';

@Component({
  selector: 'itg-root',
  template: `
    
    <!--<router-outlet></router-outlet>-->
    <div [hidden]="!loading" class="itg-route-progress-bar">
      <md-progress-bar mode="indeterminate" color="warn"></md-progress-bar>
    </div>
    <itg-navigation></itg-navigation>
    <div>
      <router-outlet></router-outlet>
    </div>
  `,
  styleUrls: ['app.component.scss'],
  providers: [
    ProfileService,
    MembersService,
    PostService,
    AuthenticationService,
    RegisterService,
    SearchService,
    LocalStorageService
  ]
})
export class AppComponent implements AfterViewInit {
  loading;

  constructor(private _router: Router) {
      this.loading = true;
  }

  ngAfterViewInit() {
    this._router.events
      .subscribe((event) => {
        if (event instanceof NavigationStart) {
          this.loading = true;
        } else if (
          event instanceof NavigationEnd ||
          event instanceof NavigationCancel
        ) {
          setTimeout(() => {
            this.loading = false;
          }, 3000);
        }
      });
  }
}
